@extends('layouts.app')

@section('content')
<h1 class="text-center"> Edit Todos</h1>

<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card card-default">
      <div class="card-header">
        Edit Todo
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
      <form action="/todos/{{ $todo->id }}/update" method="POST">
          @csrf
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="name" value="{{ $todo->name }}">
          </div>
          <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" name="description" cols="5" rows="5" id="description" class="form-control"
              placeholder="description" >{{ $todo->description }}</textarea>
          </div>
          <div class="form-group text-center">
            <button class="btn btn-success btn-small">update Todo</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection