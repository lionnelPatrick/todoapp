@extends('layouts.app')

@section('content')
  <h1 class="text-center my-5"> {{ $todo->name}}</h1>
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card card-default">
          <div class="card-header">
            Detail
          </div>
          <div class="card-body">
            {{ $todo->description }}
          </div>
        </div>
        <a class="btn btn-warning btn-sm my-2" href="/todos/{{ $todo->id }}/edit">edit todo</a> 
        <a class="btn btn-danger btn-sm my-2" href="/todos/{{ $todo->id }}/delete">delete todo</a>
      </div>
    </div>
@endsection
