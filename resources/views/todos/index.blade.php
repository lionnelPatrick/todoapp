@extends('layouts.app')
  @section('content')
    <h1 class="text-center my-5"> TODO'S PAGE</h1>
    <div class="row justify-content-center"> 
      <div class="col-md-8">
        <div class="card card-default">
          <div class="card-header">
            Todos
          </div>
          <div class="card-body">
            <ul class="list-group">
              @foreach ($todos as $todo)
              <li class="list-group-item"> {{ $todo->name }}
                <a class="btn btn-primary btn-sm float-right" href="/todos/{{$todo->id}}"> view</a>
                @if(!$todo->completed)
                  <a class="btn btn-warning btn-sm mr-2 float-right" href="/todos/{{ $todo->id }}/complete">complete todo</a>
                @endif
              </li>
    
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  @endsection